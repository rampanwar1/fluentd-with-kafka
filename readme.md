# get kafka topic

kafka-topics.sh --bootstrap-server localhost:9092 --list 

# consume messages from kafka topic 

kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic fluentdlogs

kafka-topics.sh --bootstrap-server localhost:9092 --topic fluentdlogs --create --partitions 3 --replication-factor 1 