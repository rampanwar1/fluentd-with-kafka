FROM fluent/fluentd:v1.11-debian

USER root

RUN gem install fluent-plugin-elasticsearch

RUN gem install fluent-plugin-s3

RUN fluent-gem install fluent-plugin-kafka

